/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.daw2.combinaciones;

/**
 * Clase que nos permite almacenar dos valores del tipo que queramos
 * @author https://stackoverflow.com/a/677248
 */
public class Pair<A, B> {
    private A first;
    private B second;

    /**
     * Crea una pareja que almacena en first el primer elemento y en second el segundo
     * @param first Primer elemento
     * @param second Segundo elemento
     */
    public Pair(A first, B second) {
        super();
        this.first = first;
        this.second = second;
    }

    /**
     * Sobreescritura del método hashCode()
     * @return HashCode del objeto
     */
    public int hashCode() {
        int hashFirst = first != null ? first.hashCode() : 0;
        int hashSecond = second != null ? second.hashCode() : 0;

        return (hashFirst + hashSecond) * hashSecond + hashFirst;
    }

    /**
     * Devuelve true si ambos Pair contienen los mismos valores en first y second
     * @param other Pair con el que comparar
     * @return true si ambos Pair contienen los mismos valores en first y second
     */
    public boolean equals(Object other) {
        if (other instanceof Pair) {
            Pair otherPair = (Pair) other;
            return 
            ((  this.first == otherPair.first ||
                ( this.first != null && otherPair.first != null &&
                  this.first.equals(otherPair.first))) &&
             (  this.second == otherPair.second ||
                ( this.second != null && otherPair.second != null &&
                  this.second.equals(otherPair.second))) );
        }

        return false;
    }

    /**
     * Muestra el contenido del pair en formato (first, second)
     * @return 
     */
    public String toString()
    { 
           return "(" + first + ", " + second + ")"; 
    }

    /**
     * Devuelve el valor almacenado en first
     * @return first
     */
    public A getFirst() {
        return first;
    }

    public void setFirst(A first) {
        this.first = first;
    }
    /**
     * Devuelve el valor almacenado en second
     * @return second
     */
    public B getSecond() {
        return second;
    }

    public void setSecond(B second) {
        this.second = second;
    }
}


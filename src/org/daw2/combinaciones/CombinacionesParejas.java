/*
 * Copyright 2022 Rafael González Centeno.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.daw2.combinaciones;

/**
 * Adaptación del algoritmo encontrado en la URL: <a href="https://github.com/samuelmgalan/CalendarioLiga/blob/master/src/src/Calendario.java">https://github.com/samuelmgalan/CalendarioLiga/blob/master/src/src/Calendario.java</a>
 * @author Rafael González Centeno, samuelmgalan
 */
public class CombinacionesParejas {
    private CombinacionesParejas(){}
    /**
     * Devuelve una matriz en la que cada fila contiene las combinaciones de cada jornada. Cada Pair representa en first el índice+1 en la lista del equipo local y en second el del visitante
     * @param N Número de equipos en la liga
     * @return matriz en la que cada fila contiene las combinaciones de cada jornada. Cada Pair representa en first el índice+1 en la lista del equipo local y en second el del visitante
     */
    public static Pair<Integer, Integer>[][] Calendario(int N){
        int[] equipos;
	String[][] matriz1,matriz2;
        Pair<Integer, Integer>[][] jornadas;
        equipos = new int[N];
        //Asigno posiciones del array a los equipos
        for (int i=0;i<N;i++){
                equipos[i] = i + 1;
        }

        matriz1 = new String[N-1][N/2];
        matriz2 = new String[N-1][N/2];
        jornadas = new Pair[N-1][N/2]; //primera vuelta        

        //Relleno las matrices
        /*   Matriz 1    	 Matriz 2			 
                1   2   3		6   5   4
                4   5   1		6   3   2
                2   3   4		6   1   5
                5   1   2		6   4   3
                3   4   5		6   2   1

                Resultado:

                J1	6vs1	2vs5	3vs4
                J2	4vs6	5vs3	1vs2
                J3	6vs2	3vs1	4vs5
                J4	5vs6	1vs4	2vs3
                J5	6vs3	4vs2	5vs1
         */

        int cont = 0;
        int cont2 = N-2;

        for(int i=0;i<N-1;i++){
                for(int j=0;j<N/2;j++){
                        //matriz1
                        matriz1[i][j] = String.valueOf(equipos[cont]);
                        cont++;
                        if(cont==(N-1)) cont=0;

                        //matriz2
                        if(j==0) matriz2[i][j] = String.valueOf(N);
                        else {
                                matriz2[i][j] = String.valueOf(equipos[cont2]);
                                cont2--;
                                if(cont2==-1) cont2 = N-2;
                        }

                        //Elaboro la matriz final de enfrentamientos por jornada (primera vuelta)
                        if(j==0){
                                if(i%2==0){
                                    jornadas[i][j] = new Pair<>(Integer.valueOf(matriz2[i][j]) , Integer.valueOf(matriz1[i][j]));
                                }
                                else jornadas[i][j] = new Pair<>(Integer.valueOf(matriz1[i][j]) , Integer.valueOf(matriz2[i][j])) ;
                        }
                        else jornadas[i][j] = new Pair<>( Integer.valueOf(matriz1[i][j]), Integer.valueOf(matriz2[i][j]));

                        

                }
        }        

        System.out.println();        
        return jornadas;
    }
    
    /**
     * Muestra por pantalla el resultado devuelto por la función Calendario
     * @param jornadas Matriz de pares
     */
    public static void mostrar(Pair<Integer, Integer>[][] jornadas){
        //Solo para mostrarlo por consola

        int jorn = 1;
        int N = jornadas.length + 1;
        for(int i=0;i<N-1;i++){
                for(int j=0;j<N/2;j++){
                        System.out.print("J"+jorn+" "+jornadas[i][j] + "\t"); 
                        if(j==(N/2)-1) System.out.println();
                }
                jorn++;
        }
    }
}
